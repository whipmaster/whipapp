CREATE TABLE workout_detail
  (
    workout_id                    SERIAL,
    workout_detail_id             SERIAL,
    workout_detail_exercise_id    SERIAL, 
    workout_detail_exercise_super_parent_id    INTEGER, 
    workout_detail_stage          VARCHAR(100),
    workout_detail_sequence       INTEGER not null,
    workout_detail_rest_time      INTEGER,
    workout_detail_reps           INTEGER not null,
    workout_detail_sets           JSONB,
    workout_detail_percent_of_max INTEGER,
    workout_detail_optional_flag  VARCHAR(1) CHECK (workout_detail_optional_flag in ('Y', 'N')),
    PRIMARY KEY(workout_id,workout_detail_sequence),
    CONSTRAINT fk_workout_detail_id FOREIGN KEY (workout_id) REFERENCES workout (workout_id),
    CONSTRAINT fk_workout_detail_exercise FOREIGN KEY (workout_detail_exercise_id) REFERENCES exercise (exercise_id),
    CONSTRAINT fk_workout_detail_exercise_super_parent_id FOREIGN KEY (workout_detail_exercise_super_parent_id) REFERENCES exercise (exercise_id)
    ON DELETE CASCADE
  );
