CREATE TABLE equipment
  (
    equipment_id serial PRIMARY KEY,
    equipment_description VARCHAR(1000)
  );
