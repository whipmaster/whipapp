CREATE TABLE program_tag
  (
    program_tag_id serial PRIMARY KEY,
    program_id serial,
    tag_id serial,
    CONSTRAINT fk_program_tag_program FOREIGN KEY (program_id) REFERENCES program (program_id),
    CONSTRAINT fk_program_tag_tag FOREIGN KEY (tag_id) REFERENCES tag (tag_id)
  );

