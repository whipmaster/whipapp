CREATE TABLE athlete
  (
    athlete_id               SERIAL PRIMARY KEY,
    athlete_first_name       VARCHAR(1000),
    athlete_last_name        VARCHAR(1000),
    athlete_middle_name      VARCHAR(1000),
    athlete_dob              DATE,
    athlete_phone_number     VARCHAR(1000),
    athlete_address_line_1   VARCHAR(1000),
    athlete_address_line_2   VARCHAR(1000),
    athlete_city             VARCHAR(1000),
    athlete_state            VARCHAR(2),
    athlete_zip              VARCHAR(1000),
    athlete_nation           VARCHAR(1000),
    athletet_grad_year       VARCHAR(4),
    athlete_grad_date        DATE,
    athlete_picture_location VARCHAR(1000)
  );
