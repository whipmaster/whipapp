CREATE TABLE program
  (
    program_id          SERIAL PRIMARY KEY,
    program_name        VARCHAR(1000),
    program_description VARCHAR(1000),
    program_start_date  DATE,
    program_end_date    DATE,
    program_notes       VARCHAR(1000)
  );

