CREATE TABLE test_result
  (
    test_result_id SERIAL PRIMARY KEY,
    test_result_athlete_id      SERIAL,
    test_result_exercise_id    SERIAL,
    test_result_uom_id         SERIAL,
    test_result_measure        VARCHAR(1000),
    CONSTRAINT fk_test_result_athlete_id FOREIGN KEY (test_result_athlete_id) REFERENCES athlete (athlete_id),
    CONSTRAINT fk_test_result_exercise_id FOREIGN KEY (test_result_exercise_id) REFERENCES exercise (exercise_id)
    ON DELETE CASCADE,
    CONSTRAINT fk_test_result_uom_id FOREIGN KEY (test_result_uom_id) REFERENCES uom (uom_id)        
  );
