CREATE TABLE athlete_injury
  (
    athlete_injury_injury_id SERIAL,
    athlete_injury_athlete_id SERIAL,
    athlete_injury_injury_date         DATE,
    athlete_injury_injury_removed_date DATE,
    CONSTRAINT fk_program_athlete_injury_injury_id FOREIGN KEY (athlete_injury_injury_id) REFERENCES injury (injury_id),
    CONSTRAINT fk_program_athlete_injury_athlete_id FOREIGN KEY (athlete_injury_athlete_id) REFERENCES athlete (athlete_id)
  );
