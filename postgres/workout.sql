CREATE TABLE workout
  (
    workout_id serial PRIMARY KEY,
    workout_name          VARCHAR(1000) NOT NULL CHECK (workout_name <> ''),
    workout_description   VARCHAR(2000),
    workout_instructions  VARCHAR(2000),
    workout_optional_flag VARCHAR(1) NOT NULL CHECK (workout_optional_flag in ('Y', 'N'))
  );
