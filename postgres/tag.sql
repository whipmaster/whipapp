CREATE TABLE tag
  (tag_id serial PRIMARY KEY,
   tag_key VARCHAR(1000),
   tag_value VARCHAR(1000)
  );
