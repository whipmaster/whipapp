CREATE TABLE workout_tag
  (
    workout_tag_id serial PRIMARY KEY,
    workout_id serial,
    tag_id serial,
    CONSTRAINT fk_workout_tag_workout FOREIGN KEY (workout_id) REFERENCES workout (workout_id),
    CONSTRAINT fk_workout_tag_tag FOREIGN KEY (tag_id) REFERENCES tag (tag_id)
  );

