CREATE TABLE exercise
  (
    exercise_id serial PRIMARY KEY,
    exercise_name  VARCHAR(1000) NOT NULL CHECK (exercise_name <> ''),
    exercise_description  VARCHAR(1000),
    exercise_uom_id serial,
    exercise_testable VARCHAR(1) NOT NULL CHECK (exercise_testable in ('Y', 'N')),
    CONSTRAINT fk_exercise_uom FOREIGN KEY (exercise_uom_id) REFERENCES uom (uom_id)
  );
