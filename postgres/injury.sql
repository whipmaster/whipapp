CREATE TABLE injury
  (
    injury_id   SERIAL PRIMARY KEY,
    injury_name VARCHAR(1000)
  );
