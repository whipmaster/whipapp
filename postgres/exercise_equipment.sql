CREATE TABLE exercise_equipment
  (
    exercise_equipment_id SERIAL,
    exercise_equipment_exercise_id           SERIAL,
    exercise_equipment_equipment_id          SERIAL,
    primary key(exercise_equipment_exercise_id, exercise_equipment_equipment_id),
    CONSTRAINT fk_exercise_equipment_exercise_id FOREIGN KEY (exercise_equipment_exercise_id) REFERENCES exercise (exercise_id) ON DELETE CASCADE,
    CONSTRAINT fk_exercise_equipment_equipment_id FOREIGN KEY (exercise_equipment_equipment_id) REFERENCES equipment (equipment_id) ON DELETE CASCADE
  );
