set schema 'dev';
\i injury.sql
\i uom.sql 
\i tag.sql
\i athlete.sql
\i workout.sql
\i equipment.sql
\i athlete_injury.sql
\i workout_tag.sql
\i exercise.sql
\i exercise_tag.sql
\i workout_detail.sql
\i program.sql
\i athlete_program.sql
\i program_tag.sql
\i program_detail.sql
\i test_result.sql
\i exercise_equipment.sql
