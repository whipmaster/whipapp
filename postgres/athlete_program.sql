CREATE TABLE athlete_program
  (
    athlete_id         SERIAL,
    program_id         SERIAL,
    athlete_start_date DATE,
    athlete_end_date   DATE,
    CONSTRAINT fk_athlete_program_athlete_id FOREIGN KEY (athlete_id) REFERENCES athlete (athlete_id),
    CONSTRAINT fk_athlete_program_program_id FOREIGN KEY (program_id) REFERENCES program (program_id)
  );
