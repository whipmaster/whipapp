drop schema dev cascade;
create schema dev;
set schema 'dev';
\i compile.sql
\i dev_uom.sql;
\i dev_equipment.sql;
\i dev_exercise.sql;
\i dev_exercise_equipment.sql;
\i dev_workout.sql;
\i dev_workout_detail.sql

