CREATE TABLE program_detail
  (
    program_id                     SERIAL,
    program_detail_id              SERIAL PRIMARY KEY,
    program_detail_exercise_id     SERIAL,
    program_detail_sequence_number INTEGER,
    program_detail_date            DATE,
    program_detail_reps            INTEGER,
    program_detail_sets           JSONB,
    program_detail_rest_time      INTEGER,
    program_detail_percent_of_max INTEGER,
    program_detail_workout_id     SERIAL,
    program_detail_optional_flag  VARCHAR(1),
    CONSTRAINT fk_program_detail_exercise_id FOREIGN KEY (program_detail_exercise_id) REFERENCES exercise (exercise_id) ON DELETE CASCADE,
    CONSTRAINT fk_program_detail_program_id FOREIGN KEY (program_id) REFERENCES program (program_id),
    CONSTRAINT fk_program_detail_workout_id FOREIGN KEY (program_detail_workout_id) REFERENCES workout (workout_id)
  );

