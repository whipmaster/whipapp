CREATE TABLE exercise_tag
  (
    exercise_tag_id serial PRIMARY KEY,
    exercise_id serial,
    tag_id serial,
    CONSTRAINT fk_excercise_tag_exercise FOREIGN KEY (exercise_id) REFERENCES exercise (exercise_id)
    ON DELETE CASCADE,
    CONSTRAINT fk_excercise_tag_tag FOREIGN KEY (tag_id) REFERENCES tag (tag_id)
  );
