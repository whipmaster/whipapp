const path = require('path');
const db = require('./database');
const pgp = db.$config.pgp;
// add query functions

module.exports = {
  getAllWorkoutDetails: getAllWorkoutDetails,
  getOneWorkoutDetail: getOneWorkoutDetail,
  createWorkoutDetail: createWorkoutDetail,
  updateWorkoutDetail: updateWorkoutDetail,
  deleteWorkoutDetail: deleteWorkoutDetail
};

// Helper for linking to external query files:
function sql(file) {
  const fullPath = path.join(__dirname, file);
  return new pgp.QueryFile(fullPath, {
    minify: true
  });
}

const sqlgetAllWorkoutDetails = sql('./queries/workoutDetail/getAllWorkoutDetail.sql');
const sqlgetOneWorkoutDetail = sql('./queries/workoutDetail/getOneWorkoutDetail.sql');
const sqlupdateWorkoutDetail = sql('./queries/workoutDetail/updateWorkoutDetail.sql');
const sqlcreateWorkoutDetail = sql('./queries/workoutDetail/createWorkoutDetail.sql');
const sqldeleteWorkoutDetail = sql('./queries/workoutDetail/deleteWorkoutDetail.sql');





function getAllWorkoutDetails(req, res, next) {
  db.any(sqlgetAllWorkoutDetails, {
      user_schema: req.session.schema_name,
      workout_id: parseInt(req.params.id)
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'All the details'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function getOneWorkoutDetail(req, res, next) {
  db.one(sqlgetOneWorkoutDetail, {
      user_schema: req.session.schema_name,
      workout_id: parseInt(req.params.id),
      workout_detail_id: parseInt(req.params.detailId)
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'One workout details'
        });
    })
    .catch(function(err) {
      //Get one needs to have the 404 in the error handler as the query throws an error
      //instead of just returning 0 rows due to the sql parsing and binding from the external file
      if (err.message === "No data returned from the query.") {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout Detail does not exist'
          });
        return;
      }
      return next(err);
    });
};

function createWorkoutDetail(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.workout_id = parseInt(req.params.id);
  db.none(sqlcreateWorkoutDetail, req.body)
    .then(function() {
      res.status(200)
        .json({
          status: 'Success',
          message: 'Inserted workout detail'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function updateWorkoutDetail(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.workout_id = parseInt(req.params.id);
  req.body.workout_detail_id = parseInt(req.params.detailId);
  db.result(sqlupdateWorkoutDetail, req.body)
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout detail does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: 'Updated workout detail'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function deleteWorkoutDetail(req, res, next) {

  db.result(sqldeleteWorkoutDetail, {
      user_schema: req.session.schema_name,
      workout_id: parseInt(req.params.id),
      workout_detail_id: parseInt(req.params.detailId)
    })
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: `Deleted ${result.rowCount} workout details`
        });
    })
    .catch(function(err) {
      return next(err);
    });
};
