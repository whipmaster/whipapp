UPDATE $[user_schema^].workout
SET workout_name      = $[workout_name],
  workout_description = $[workout_description],
  workout_instructions      = $[workout_instructions],
  workout_optional_flag    = $[workout_optional_flag]
WHERE workout_id      = $[workout_id];
