INSERT
INTO $[user_schema^].workout
  (
    workout_name,
    workout_description,
    workout_instructions,
    workout_optional_flag
  )
  VALUES
  (
    $[workout_name],
$[workout_description],
$[workout_instructions],
$[workout_optional_flag]
);
