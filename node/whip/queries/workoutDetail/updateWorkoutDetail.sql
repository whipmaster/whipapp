UPDATE $[user_schema^].workout_detail
SET workout_detail_exercise_id      = $[workout_detail_exercise_id],
  workout_detail_exercise_super_parent_id = $[workout_detail_exercise_super_parent_id],
  workout_detail_stage      = $[workout_detail_stage],
  workout_detail_sequence    = $[workout_detail_sequence],
  workout_detail_rest_time    = $[workout_detail_rest_time],
  workout_detail_reps    = $[workout_detail_reps],
  workout_detail_sets    = $[workout_detail_sets],
  workout_detail_percent_of_max    = $[workout_detail_percent_of_max],
  workout_detail_optional_flag    = $[workout_detail_optional_flag]
WHERE workout_detail_id      = $[workout_detail_id]
AND workout_id = $[workout_id];
