-- Script to select all exercises
/*
SELECT *
FROM $[user_schema^].exercise
LEFT JOIN $[user_schema^].equipment
ON exercise_equipment_id = equipment_id
JOIN $[user_schema^].uom
ON exercise_uom_id = uom_id;
*/
with exercise as(
INSERT
INTO $[user_schema^].exercise
  (
    exercise_name,
    exercise_description,
    exercise_uom_id,
    exercise_testable
  )
  VALUES
  (
    $[exercise_name],
$[exercise_description],
$[exercise_uom_id],
$[exercise_testable]
)
RETURNING exercise_id)
INSERT
INTO $[user_schema^].exercise_equipment
  (
    exercise_equipment_exercise_id,
    exercise_equipment_equipment_id
  )
  VALUES
  (
    (select exercise_id
from exercise),
--The ::integer[] notation is a cast to let postgres know what datatype to
--expect when a null array is passed. See issue 384 of pg-promise */
    unnest($[equipment_id]::integer[])
  );
