UPDATE $[user_schema^].exercise
SET exercise_name      = $[exercise_name],
  exercise_description = $[exercise_description],
  exercise_uom_id      = $[exercise_uom_id],
  exercise_testable    = $[exercise_testable]
WHERE exercise_id      = $[exercise_id];
DELETE FROM $[user_schema^].exercise_equipment
WHERE exercise_equipment_exercise_id = $[exercise_id];
/* The below "<> ALL" statement is checking for all rows that weren't sent
with the request
The ::integer[] notation is a cast to let postgres know what datatype to
expect when a null array is passed. See issue 384 of pg-promise */
--AND exercise_equipment_equipment_id <> ALL ($[equipment_id]);
with equipment as (select unnest($[equipment_id]::integer[]) equipment_id)
INSERT
INTO $[user_schema^].exercise_equipment
  (
    exercise_equipment_exercise_id,
    exercise_equipment_equipment_id
  )
  select  $[exercise_id],
    equipment_id from equipment
--This is a hack to not insert when a valid exercise isn't sent
   where exists(select 1 from $[user_schema^].exercise where exercise_id = $[exercise_id])
   and equipment_id is not null
   ON conflict DO nothing;
--This is a hack to get the 404 working
select 1 from $[user_schema^].exercise where exercise_id = $[exercise_id];
