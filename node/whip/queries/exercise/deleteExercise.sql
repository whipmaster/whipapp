-- Script to select all exercises
/*
SELECT *
FROM $[user_schema^].exercise
LEFT JOIN $[user_schema^].equipment
ON exercise_equipment_id = equipment_id
JOIN $[user_schema^].uom
ON exercise_uom_id = uom_id;
*/

DELETE 
FROM $[user_schema^].exercise
WHERE exercise_id = $[exercise_id];
