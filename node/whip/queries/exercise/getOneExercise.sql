-- Script to select all exercises
/*
SELECT *
FROM $[user_schema^].exercise
LEFT JOIN $[user_schema^].equipment
ON exercise_equipment_id = equipment_id
JOIN $[user_schema^].uom
ON exercise_uom_id = uom_id;
*/

SELECT exercise.*,
  uom.*,
  array_agg(equipment_id)  exercise_equipment,
  array_agg(equipment_description) equipment_description
FROM $[user_schema^].exercise
JOIN $[user_schema^].uom
ON exercise_uom_id = uom_id
LEFT JOIN $[user_schema^].exercise_equipment
ON exercise_equipment_exercise_id = exercise_id
LEFT JOIN $[user_schema^].equipment
ON exercise_equipment_equipment_id = equipment_id
WHERE exercise_id = $[exercise_id]
GROUP BY exercise_id, uom_id;

