UPDATE $[user_schema^].equipment
SET equipment_description = $[equipment_description]
WHERE equipment_id = $[equipment_id];
