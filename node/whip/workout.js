const path = require('path');
const db = require('./database');
const pgp = db.$config.pgp;
// add query functions

module.exports = {
  getAllWorkouts: getAllWorkouts,
  getOneWorkout: getOneWorkout,
  createWorkout: createWorkout,
  updateWorkout: updateWorkout,
  deleteWorkout: deleteWorkout
};

// Helper for linking to external query files:
function sql(file) {
  const fullPath = path.join(__dirname, file);
  return new pgp.QueryFile(fullPath, {
    minify: true
  });
}

const sqlgetAllWorkouts = sql('./queries/workout/getAllWorkout.sql');
const sqlgetOneWorkouts = sql('./queries/workout/getOneWorkout.sql');
const sqlupdateWorkout = sql('./queries/workout/updateWorkout.sql');
const sqlcreateWorkout = sql('./queries/workout/createWorkout.sql');
const sqldeleteWorkout = sql('./queries/workout/deleteWorkout.sql');





function getAllWorkouts(req, res, next) {
  db.any(sqlgetAllWorkouts, {
      user_schema: req.session.schema_name
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'All the workouts'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function getOneWorkout(req, res, next) {
  db.one(sqlgetOneWorkouts, {
      user_schema: req.session.schema_name,
      workout_id: req.params.id
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'One workout'
        });
    })
    .catch(function(err) {
      //Get one needs to have the 404 in the error handler as the query throws an error
      //instead of just returning 0 rows due to the sql parsing and binding from the external file
      if (err.message === "No data returned from the query.") {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout does not exist'
          });
        return;
      }
      return next(err);
    });
};

function createWorkout(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  db.none(sqlcreateWorkout, req.body)
    .then(function() {
      res.status(200)
        .json({
          status: 'Success',
          message: 'Inserted workout'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function updateWorkout(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.equipment_id = a => a.workout_equipment.equipment_id;
  req.body.workout_id = parseInt(req.params.id);
  db.result(sqlupdateWorkout, req.body)
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: 'Updated workout'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function deleteWorkout(req, res, next) {
  var workoutId = parseInt(req.params.id);
  var schema = req.session.schema_name;
  db.result(sqldeleteWorkout, {
      user_schema: req.session.schema_name,
      workout_id: parseInt(req.params.id)
    })
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Workout does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: `Deleted ${result.rowCount} workout`
        });
    })
    .catch(function(err) {
      return next(err);
    });
};
