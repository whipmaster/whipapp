const pgp = require('pg-promise')();

const cn = {
  host: process.env.PGHOST,
  port: 5432,
  database: process.env.PGDATABSE,
  user: process.env.PGUSER
};
var db = pgp(cn);

module.exports = db;
