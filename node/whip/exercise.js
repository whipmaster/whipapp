const path = require('path');
const db = require('./database');
const pgp = db.$config.pgp;
// add query functions

module.exports = {
  getAllExercises: getAllExercises,
  getOneExercise: getOneExercise,
  createExercise: createExercise,
  updateExercise: updateExercise,
  deleteExercise: deleteExercise
};

// Helper for linking to external query files:
function sql(file) {
  const fullPath = path.join(__dirname, file);
  return new pgp.QueryFile(fullPath, {
    minify: true
  });
}

const sqlgetAllExercises = sql('./queries/exercise/getAllExercises.sql');
const sqlgetOneExercises = sql('./queries/exercise/getOneExercise.sql');
const sqlupdateExercise = sql('./queries/exercise/updateExercise.sql');
const sqlcreateExercise = sql('./queries/exercise/createExercise.sql');
const sqldeleteExercise = sql('./queries/exercise/deleteExercise.sql');





function getAllExercises(req, res, next) {
  db.any(sqlgetAllExercises, {
      user_schema: req.session.schema_name,
      url: req.protocol + '://' + req.get('host') + req.originalUrl
//      url : req.originalUrl
    })
    .then(function(data) {
      var exercise = {};
      data.forEach(function(element) {
        element.links = {"rel" : "self", "href" : req.protocol + '://' + req.get('host') + req.originalUrl + "/" + element.exercise_id, "type": "GET" }
});
//      exercise.exercise_id = data[0].equipment_description[4];
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'All the exercises'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function getOneExercise(req, res, next) {
  db.one(sqlgetOneExercises, {
      user_schema: req.session.schema_name,
      exercise_id: req.params.id
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'One exercise'
        });
    })
    .catch(function(err) {
      //Get one needs to have the 404 in the error handler as the query throws an error
      //instead of just returning 0 rows due to the sql parsing and binding from the external file
      if (err.message === "No data returned from the query.") {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Exercise does not exist'
          });
        return;
      }
      return next(err);
    });
};

function createExercise(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.equipment_id = a => a.exercise_equipment.equipment_id;
  db.none(sqlcreateExercise, req.body)
    .then(function() {
      res.status(200)
        .json({
          status: 'Success',
          message: 'Inserted exercise'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function updateExercise(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.equipment_id = a => a.exercise_equipment.equipment_id;
  req.body.exercise_id = parseInt(req.params.id);
  db.result(sqlupdateExercise, req.body)
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Exercise does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: 'Updated exercise'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function deleteExercise(req, res, next) {
  var exerciseId = parseInt(req.params.id);
  var schema = req.session.schema_name;
  db.result(sqldeleteExercise, {
      user_schema: req.session.schema_name,
      exercise_id: parseInt(req.params.id)
    })
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Exercise does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: `Deleted ${result.rowCount} exercise`
        });
    })
    .catch(function(err) {
      return next(err);
    });
};
