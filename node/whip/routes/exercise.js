var express = require('express');
var router = express.Router();

var db = require('../database');
var exercise = require('../exercise');
var login = require('../login');

//login.checkAccessToken(

//console.log(req.body);
//router.get('/',  db.getAllExercises);


router.get('/', exercise.getAllExercises);
router.get('/:id', exercise.getOneExercise);
router.post('/', exercise.createExercise);
router.put('/:id', exercise.updateExercise);
router.delete('/:id', exercise.deleteExercise);

/* if(login.checkAccessToken(req.body)){
if(true){
console.log('authed');
 db.getAllExercises();
//}
console.log('failed');
// res.send('yeah this did not work');

});
*/

module.exports = router;
