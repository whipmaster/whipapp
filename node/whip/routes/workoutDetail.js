var express = require('express');
var router = express.Router();

var db = require('../database');
var workout = require('../workoutDetail');
var login = require('../login');

//login.checkAccessToken(

//console.log(req.body);
//router.get('/',  db.getAllWorkouts);


router.get('/:id', workout.getAllWorkoutDetails);
router.get('/:id/:detailId', workout.getOneWorkoutDetail);
router.post('/:id', workout.createWorkoutDetail);
router.put('/:id/:detailId', workout.updateWorkoutDetail);
router.delete('/:id/:detailId', workout.deleteWorkoutDetail);

/* if(login.checkAccessToken(req.body)){
if(true){
console.log('authed');
 db.getAllWorkouts();
//}
console.log('failed');
// res.send('yeah this did not work');

});
*/

module.exports = router;
