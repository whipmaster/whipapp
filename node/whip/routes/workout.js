var express = require('express');
var router = express.Router();

var db = require('../database');
var workout = require('../workout');
var login = require('../login');

//login.checkAccessToken(

//console.log(req.body);
//router.get('/',  db.getAllWorkouts);


router.get('/', workout.getAllWorkouts);
router.get('/:id', workout.getOneWorkout);
router.post('/', workout.createWorkout);
router.put('/:id', workout.updateWorkout);
router.delete('/:id', workout.deleteWorkout);

/* if(login.checkAccessToken(req.body)){
if(true){
console.log('authed');
 db.getAllWorkouts();
//}
console.log('failed');
// res.send('yeah this did not work');

});
*/

module.exports = router;
