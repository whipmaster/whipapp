var express = require('express');
var router = express.Router();

var db = require('../database');
var equipment = require('../equipment');
var login = require('../login');

//login.checkAccessToken(

//console.log(req.body);
//router.get('/',  db.getAllEquipments);


router.get('/', equipment.getAllEquipments);
router.get('/:id', equipment.getOneEquipment);
router.post('/', equipment.createEquipment);
router.put('/:id', equipment.updateEquipment);
router.delete('/:id', equipment.deleteEquipment);

/* if(login.checkAccessToken(req.body)){
if(true){
console.log('authed');
 db.getAllEquipments();
//}
console.log('failed');
// res.send('yeah this did not work');

});
*/

module.exports = router;
