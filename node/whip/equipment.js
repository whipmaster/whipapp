const path = require('path');
const db = require('./database');
const pgp = db.$config.pgp;
// add query functions

module.exports = {
  getAllEquipments: getAllEquipments,
  getOneEquipment: getOneEquipment,
  createEquipment: createEquipment,
  updateEquipment: updateEquipment,
  deleteEquipment: deleteEquipment
};

// Helper for linking to external query files:
function sql(file) {
  const fullPath = path.join(__dirname, file);
  return new pgp.QueryFile(fullPath, {
    minify: true
  });
}

const sqlgetAllEquipments = sql('./queries/equipment/getAllEquipments.sql');
const sqlgetOneEquipments = sql('./queries/equipment/getOneEquipment.sql');
const sqlupdateEquipment = sql('./queries/equipment/updateEquipment.sql');
const sqlcreateEquipment = sql('./queries/equipment/createEquipment.sql');
const sqldeleteEquipment = sql('./queries/equipment/deleteEquipment.sql');





function getAllEquipments(req, res, next) {
  db.any(sqlgetAllEquipments, {
      user_schema: req.session.schema_name
    })
    .then(function(data) {
      var equipment = {};
      equipment.equipment_id = data[0].equipment_description[4];
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'All the equipments'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function getOneEquipment(req, res, next) {
  db.one(sqlgetOneEquipments, {
      user_schema: req.session.schema_name,
      equipment_id: req.params.id
    })
    .then(function(data) {
      res.status(200)
        .json({
          status: 'Success',
          data: data,
          message: 'One equipment'
        });
    })
    .catch(function(err) {
      //Get one needs to have the 404 in the error handler as the query throws an error
      //instead of just returning 0 rows due to the sql parsing and binding from the external file
      if (err.message === "No data returned from the query.") {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Equipment does not exist'
          });
        return;
      }
      return next(err);
    });
};

function createEquipment(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.equipment_id = a => a.equipment_equipment.equipment_id;
  db.none(sqlcreateEquipment, req.body)
    .then(function() {
      res.status(200)
        .json({
          status: 'Success',
          message: 'Inserted equipment'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function updateEquipment(req, res, next) {
  req.body.user_schema = req.session.schema_name;
  req.body.equipment_id = a => a.equipment_equipment.equipment_id;
  req.body.equipment_id = parseInt(req.params.id);
  db.result(sqlupdateEquipment, req.body)
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Equipment does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: 'Updated equipment'
        });
    })
    .catch(function(err) {
      return next(err);
    });
};


function deleteEquipment(req, res, next) {
  var equipmentId = parseInt(req.params.id);
  var schema = req.session.schema_name;
  db.result(sqldeleteEquipment, {
      user_schema: req.session.schema_name,
      equipment_id: parseInt(req.params.id)
    })
    .then(function(result) {

      if (result.rowCount === 0) {
        res.status(404)
          .json({
            status: 'Failed',
            message: 'Equipment does not exist'
          });
        return;
      }

      res.status(200)
        .json({
          status: 'Success',
          message: `Deleted ${result.rowCount} equipment`
        });
    })
    .catch(function(err) {
      return next(err);
    });
};
