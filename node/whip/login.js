const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
const jwks = {
  "keys": [{
    "alg": "RS256",
    "e": "AQAB",
    "kid": "izKWh0KRv9tusr6gYROgvnQYrbEx43mJ+RwGLoEDt24=",
    "kty": "RSA",
    "n": "ze523l85_mDFNisp0HNwNNbW8MAZdZkLBeveIHpD3zdbHQvSgz8HKoSucambbCUCeIC7a9vtTU762ngvKsHBZM1NbmPqb6WyOZX4h6R61UXKJS2YHj0WxnWCA8APLky7IKJaPhHpzEOKc8OGcNKDBQQu0OKcZdSzFrzBNJvsNBAr5t88nYR75RpKyazOvDPCi0sPq6_Lqo9fiODtMZCAwiBFoiL9pMadNIB67mDHqcy3CAFzAsZkEbV2nqm6yOrqhkcAcNR-5TPAEcx8f8U4asIvvc8TnIIdATXLlqbdfCtrb56W4emEQ1ngDos13AHJ0RJi_tJXKOoQUtlhdvenuQ",
    "use": "sig"
  }, {
    "alg": "RS256",
    "e": "AQAB",
    "kid": "Me6HazrVzZ8E1IrRBTP/2zYjf0lPTq1QT3hTgLo6Arc=",
    "kty": "RSA",
    "n": "udgqafJHROzPV5nVxfuI5Y4vp1EAwnGRJ5G6wzNHKkoqX0KkCd210dRR2697oDyiLxvw_aZg8YgxlCApBWKmjDBSDBE9MmWQkOiSVv6hi0uDPa2W2sBjKZgfyLnlI3KCUKmM2kk7KdbV0vLe2gDWWUuKes1LQgLoyIA1pZJtw4u2_6KqEyvRyTE5qaOb60TsDXgQbHryzABD2i8LrPpNxDZBK_4GcwHmpr6LHLzKvXaaQ0uvMbI4ob30JNtstGfa8SNdNNlcdbQLdQR4VJ5Zuy0NfLobM-3LvTk-aBbopHAwwJbXaxbyFBnX1_P03TKuVjAUFVjZ-SbURAapchcsIw",
    "use": "sig"
  }]
};

var redirectURL = 'http://www.gotanybeer.com.s3-website.us-east-2.amazonaws.com/dev/';

module.exports = {
  checkAuth: checkAuth,
  setCookie: setCookie
};

function checkAuth(req, res, next) {
  //Set up the id token and access token from the provided cookies
  var access = req.cookies['access'];
  var id = req.cookies['id'];

  // If we didn't get one of the two tokens then redirect
  // fix this when we have auth implemented
  if (!access || !id) {
    console.log('no token presented in cookies');
    req.session.schema_name = 'test';
    return next();
    //return res.redirect(redirectURL);
//
  }
  //Decode access and id tokens
  var decodedAccess = jwt.decode(access, {
    complete: true
  });
  var decodedId = jwt.decode(id, {
    complete: true
  });

  //Get the correct key from the jwks based on the kid
  var jwkAccess = jwks.keys.filter(function(v) {
    return v.kid === decodedAccess.header.kid;
  })[0];

  var jwkId = jwks.keys.filter(function(v) {
    return v.kid === decodedId.header.kid;
  })[0];

  //Convert the key to pem
  var pemAccess = jwkToPem(jwkAccess);
  var pemId = jwkToPem(jwkId);

  //Verify the token with the pem
  jwt.verify(access, pemAccess, function(err, decoded) {
    if (err) {
      console.log('not authorized');
//      return res.redirect(redirectURL);
    }

    jwt.verify(id, pemId, function(err, decoded) {
      if (err) {
        console.log('not authorized');
//        return res.redirect(redirectURL);
      }
      req.session.schema_name = decodedId.payload['custom:schema_name'];
      return next();

    });
  });
};


function setCookie(req, res, next) {
  //res.cookie('token', req.body);
  res.cookie('access', req.body.access, {
    expires: new Date(Date.now() + 900000),
    secure: false,
    domain: 'ec2-13-59-64-170.us-east-2.compute.amazonaws.com'
  });
  res.cookie('id', req.body.id, {
    expires: new Date(Date.now() + 900000),
    secure: false,
    domain: 'ec2-13-59-64-170.us-east-2.compute.amazonaws.com'
  });
  return res.sendStatus(200);
};
